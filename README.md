
## Install

First we need to download Rsa.php directly and include it.

```php
include 'Rsa.php';
```

## Create key

```php
// public key
$public_key = 'YOUR_PUBLIC_KEY';
$public_key = Rsa::get_public_key($public_key);

// private key
$private_key = 'YOUR_PRIVATE_KEY';
$private_key = Rsa::get_public_key($private_key);
```

## Encrypt

```php
// public key
Rsa::public_encrypt($public_key, 'STRING_TO_ENCRYPT');

// public key without base64
Rsa::public_encrypt($public_key, 'STRING_TO_ENCRYPT', false);

// private key
Rsa::private_encrypt($private_key, 'STRING_TO_ENCRYPT');

// private key without base64
Rsa::private_encrypt($private_key, 'STRING_TO_ENCRYPT', false);
```

## Decrypt

```php
// public key
Rsa::public_decrypt($private_key, 'STRING_TO_DECRYPT');

// public key without base64
Rsa::public_decrypt($private_key, 'STRING_TO_DECRYPT', false);

// private key
Rsa::private_decrypt($private_key, 'STRING_TO_DECRYPT');

// private key without base64
Rsa::private_decrypt($private_key, 'STRING_TO_DECRYPT', false);
```
