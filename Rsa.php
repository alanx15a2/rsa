<?php

class Rsa
{
    // 公钥正规化
    public static function get_public_key($key)
    {
        $public_key = "-----BEGIN PUBLIC KEY-----\r\n";
        $public_key .= wordwrap($key,64,"\r\n",TRUE) . "\r\n";
        $public_key .= "-----END PUBLIC KEY-----";

        return $public_key;
    }

    // 私钥正规化
    public static function get_private_key($key)
    {
        $private_key = "-----BEGIN PRIVATE KEY-----\r\n";
        $private_key .= wordwrap($key,64,"\r\n",TRUE) . "\r\n";
        $private_key .= "-----END PRIVATE KEY-----";

        return $private_key;
    }

    // 公钥加密
    public static function public_encrypt($key, $string, $isBase64 = true)
    {
        $public_key = openssl_pkey_get_public($key);

        if ($public_key === false) {
            return false;
        }

        openssl_public_encrypt($string, $encrypted, $public_key);
        $encrypted = ($isBase64) ? base64_encode($encrypted) : $encrypted;

        return $encrypted;
    }

    // 私钥加密
    public static function private_encrypt($key, $string, $isBase64 = true)
    {
        $private_key = openssl_pkey_get_private($key);

        if ($private_key === false) {
            return false;
        }

        openssl_private_encrypt($string, $encrypted, $private_key);
        $encrypted = ($isBase64) ? base64_encode($encrypted) : $encrypted;

        return $encrypted;
    }

    // 公钥解密
    public static function public_decrypt($key, $string, $isBase64 = true)
    {
        $public_key = openssl_pkey_get_public($key);

        if ($public_key === false) {
            return false;
        }

        $string = ($isBase64) ? base64_decode($string) : $string;
        openssl_public_decrypt($string, $crypted, $public_key);

        return $crypted;
    }

    // 私钥解密
    public static function private_decrypt($key, $string, $isBase64 = true)
    {
        $private_key = openssl_pkey_get_private($key);

        if ($private_key === false) {
            return false;
        }

        $string = ($isBase64) ? base64_decode($string) : $string;
        openssl_private_decrypt($string, $crypted, $private_key);

        return $crypted;
    }
}
